
# Cahier des charges pour Teacher Story

## Technologies envisagées
Côté serveur : Node.JS 12.18 avec Typescript et PostgreSQL   
Côté client : A définir (je penche personnellement pour Blazor en C#)

## Développement

### Serveur
-	Profil utilisateur avec niveau et état de la partie
   -	Profil : identifiants, pseudo, niveau, points, grade, chapeau
   -	Partie (JSON/YML): expérience, options, déco chez soi, objets, compétences…
   -	Jeu en cours (JSON/YML, mise à jour périodique) : tour actuel, état des élèves, du stress
-	Création de tous les évènements du cours
   -	Envoi de la seed et du document contenant les données du jeu ?
   -	Autre chose ?
-	Réception des actions
-	Définition des bonus de lvlup et des assistants disponibles
-	Définition des nouvelles classes
   -	Nature (Niveau de la classe, type de matière)
   -	Objectif
   -	Deadline => évaluation de la difficulté
   -	Récompense
   -	Elèves :
      -	Prénom et sprite
      -	Caractère
      -	Note initiale
      -	Compétence de chouchou


### Client
-	Réception du cours
-	Gestion de l’interface (animations, boutons, interactibles)
   -	Toutes les actions de cours
   -	Bonus (éponges/twinoide/batoit)
   -	Ramassage des objets
-	Envoi des actions de cours
   -	Nouvel état du document contenant les données du jeu ?
   -	Caractéristiques de l’action (dégâts, cible, bonus/malus) => modification des données côté serveur ?
-	Les ptits easters eggs
-	Envoi des actions de repos
-	Envoi des changement de la maison
   -	Même questions qu’au dessus

Divers 
-	Définir la façon de gagner des pièces
-	Envoi d’élèves
-	Classement (amis et général)
